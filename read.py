import struct, sys, os
fileList = []

def main(argv):
        fileLeng = os.path.getsize(argv[0])
        resource = open(argv[0], 'rb')

        header = resource.read(4)
        listOffset = struct.unpack('>I', resource.read(4))[0]
        resource.seek(listOffset)

        print("Header: %s  --  Offset: %i" % (header, listOffset))

        read = fileLeng - listOffset
        while read:
                filename = resource.read(15)
                fileOffset = struct.unpack('>I', resource.read(4))[0]
                fileLen = struct.unpack('>I', resource.read(4))[0]
                compr = struct.unpack('>H', resource.read(2))[0]
                fileList.append([filename, fileOffset, fileLen, compr])
                read -= 25

        for data in fileList:
                print("%s\tOffset: %i\tLength: %i\tcompression: %i" %
                        (data[0], data[1], data[2], data[3]))

if __name__ == "__main__":
        main(sys.argv[1:])
