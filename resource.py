import struct, os, sys
IMGHEADER = "RSCI"
MUSHEADER = "RSCS"
MAPHEADER = "RSCM"
fileList = []

class CompressType:
        NONE, LZ4, GZ, LZMA = range(4)

def main(argv):
        dataLength = 8

        insertList = open(argv[0], 'rb')
        for filename in insertList:
                filename = filename.rstrip('\r\n')
                fsize = os.path.getsize(filename)
                fileList.append([os.path.basename(filename),
                                dataLength, fsize, 0])
                dataLength += fsize

                if (dataLength > 2**32 - 8):
                        print("WARNING: Max resource size may be exceeded.")

        out = open("rTest.rsc", 'wb')
        out.write(IMGHEADER)
        out.write(struct.pack('>I', dataLength))

        for data in fileList:
                file = open(data[0], 'rb')
                buff = file.read()
                out.write(buff)

        for data in fileList:
                out.write(data[0][:16].zfill(15))
                out.write(struct.pack('>I', data[1]))
                out.write(struct.pack('>I', data[2]))
                out.write(struct.pack('>H', data[3]))

if __name__ == "__main__":
        main(sys.argv[1:])
